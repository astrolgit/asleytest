﻿using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCubatel.Client.Common
{
    public static class IJSExtensions
    {
        /// <summary>
        /// Msotrar Notificaciones Emergentes
        /// </summary>
        /// <param name="js"></param>
        /// <param name="texto"></param>
        /// <param name="icono">success, error, warning, info, question</param>
        /// <returns></returns>
        //[JSInvokable]
        public async static Task Notification(this IJSRuntime js, string text, string icon = "info")
        {
            await js.InvokeAsync<object>("NotificationJS", text, icon);
        }

        public async static Task<bool> Confirm(this IJSRuntime js, string title, string text)
        {
            return await js.InvokeAsync<bool>("ConfirmJS", title, text);
        }
    }
}
