#pragma checksum "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a41e749628e0a018b2f593e3c96533def7eebfbf"
// <auto-generated/>
#pragma warning disable 1591
namespace TestCubatel.Client
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using TestCubatel.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using TestCubatel.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using TestCubatel.Shared.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\Personal\1-TestCubatel\TestCubatel\Client\_Imports.razor"
using TestCubatel.Client.Common;

#line default
#line hidden
#nullable disable
    public partial class _Imports : System.Object
    {
        #pragma warning disable 1998
        protected void Execute()
        {
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime js { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient http { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationManager { get; set; }
    }
}
#pragma warning restore 1591
