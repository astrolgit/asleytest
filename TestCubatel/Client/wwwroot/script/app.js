﻿function NotificationJS(text, icon) {
    Swal.fire({
        position: 'top-end',
        type: icon,
        title: text,
        showConfirmButton: false,
        timer: 3500
    });
}

function ConfirmJS(title, text) {
    return new Promise((resolve) => {
        Swal.fire({
            title: title,
            text: text,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar'
        }).then((result) => {
            if (result.value) {
                resolve(true);
            }
            else {
                resolve(false);
            }
        });
    });
}