﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCubatel.Server.Common;
using TestCubatel.Shared;
using TestCubatel.Shared.Models;

namespace TestCubatel.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ToDoListController : ControllerBase
    {

        private readonly ApplicationDbContext _context;

        public ToDoListController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<ToDoListList>>> Get([FromQuery] Paged paged)
        {
            IQueryable<ToDoList> queryable = _context.ToDoList
                                            .Include(x => x.ToDoListItem)
                                            .OrderBy(x => x.Order);

            await HttpContext.InsertarParametrosPaginacionEnRespuesta(queryable, paged.Quantity);
            var list = await queryable.Paged(paged).ToListAsync();

            return (from x in list
                    orderby x.Order
                    select new ToDoListList
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Order = x.Order,
                        CountItem = x.ToDoListItem.Count,
                        Date = Auxiliar.DateString(x.Date),
                    }).ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ToDoList>> Get(long id)
        {
            return await _context.ToDoList
                .FirstAsync(x => x.Id == id);
        }

        [HttpGet("GetAllToDoListSelect/{idItem}")]
        public async Task<ActionResult<List<OptionSelect>>> GetAllToDoListSelect(long idItem = 0)
        {
            List<OptionSelect> response = new List<OptionSelect>();
            var lista = _context.ToDoList;
            if (lista != null && lista.Count() > 0)
            {
                response = await (from x in lista
                                   select new OptionSelect
                                   {
                                       Name = x.Name,
                                       Value = x.Id,
                                       State = x.Id == idItem ? true : false,
                                   }).ToListAsync();

            }
            if (response != null && response.Count > 0 && idItem == 0)
            {
                response[0].State = true;
            }
            return response;
        }

        [HttpPost]
        public async Task<ActionResult<ModelBasicValidad>> Post(ToDoList newItem)
        {
            if (ModelState.IsValid)
            {
                newItem.Date = DateTime.Now;
                _context.Add(newItem);
                await _context.SaveChangesAsync();

                return new ModelBasicValidad
                {
                    Message = "Lista creada satisfactoriamente.",
                    Type = "success",
                    Valid = true,
                };
            }
            return new ModelBasicValidad
            {
                Message = "Por favor verifique los datos.",
                Type = "error",
                Valid = false,
            };
        }

        [HttpPut]
        public async Task<ActionResult<ModelBasicValidad>> Put(ToDoList itemEdit)
        {
            if (ModelState.IsValid)
            {
                ToDoList oldBD = await _context.ToDoList.FindAsync(itemEdit.Id);

                if (oldBD != null)
                {
                    oldBD.Date = DateTime.Now;
                    oldBD.Name = itemEdit.Name;
                    oldBD.Order = itemEdit.Order;

                    _context.Entry(oldBD).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    return new ModelBasicValidad
                    {
                        Message = "Lista editada satisfactoriamente.",
                        Type = "success",
                        Valid = true,
                    };
                }
            }
            return new ModelBasicValidad
            {
                Message = "Por favor verifique los datos.",
                Type = "error",
                Valid = false,
            };
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            ToDoList item = new ToDoList { Id = id };
            _context.Remove(item);
            await _context.SaveChangesAsync();
            return NoContent();

        }
    }
}
