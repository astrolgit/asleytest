﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCubatel.Server.Common;
using TestCubatel.Shared;
using TestCubatel.Shared.Models;

namespace TestCubatel.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ToDoListItemController : ControllerBase
    {

        private readonly ApplicationDbContext _context;

        public ToDoListItemController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<ToDoListItemList>>> Get([FromQuery] Paged paged,
                                                                    [FromQuery] string name,
                                                                    [FromQuery] int toDoListId)
        {
            IQueryable<ToDoListItem> queryable = _context.ToDoListItem
                                                         .Include(x => x.ToDoList);

            queryable = Filter(queryable, name, toDoListId);

            await HttpContext.InsertarParametrosPaginacionEnRespuesta(queryable, paged.Quantity);
            var list = await queryable.Paged(paged).ToListAsync();

            return (from x in list
                    select new ToDoListItemList
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Status = x.Status,
                        ToDoList = x.ToDoList.Name,
                        Description = x.Description,
                        Date = Auxiliar.DateString(x.Date),
                    }).ToList();
        }

        private IQueryable<ToDoListItem> Filter(IQueryable<ToDoListItem> listBase, string name, int toDoListId)
        {
            if (!string.IsNullOrEmpty(name))
            {
                name = name.ToLower();
                listBase = listBase.Where(x => x.Name.ToLower().Contains(name));
            }
            if (toDoListId > 0)
            {
                listBase = listBase.Where(x => x.ToDoListId == toDoListId);
            }
            return listBase;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ToDoListItem>> Get(long id)
        {
            return await _context.ToDoListItem
                .FirstAsync(x => x.Id == id);
        }

        [HttpPost]
        public async Task<ActionResult<ModelBasicValidad>> Post(ToDoListItem newItem)
        {
            if (ModelState.IsValid)
            {
                newItem.Date = DateTime.Now;
                _context.Add(newItem);
                await _context.SaveChangesAsync();

                return new ModelBasicValidad
                {
                    Message = "Tarea creada satisfactoriamente.",
                    Type = "success",
                    Valid = true,
                };
            }
            return new ModelBasicValidad
            {
                Message = "Por favor verifique los datos.",
                Type = "error",
                Valid = false,
            };
        }

        [HttpPut]
        public async Task<ActionResult<ModelBasicValidad>> Put(ToDoListItem itemEdit)
        {
            if (ModelState.IsValid)
            {
                ToDoListItem oldBD = await _context.ToDoListItem.FindAsync(itemEdit.Id);

                if (oldBD != null)
                {
                    oldBD.Date = DateTime.Now;
                    oldBD.Name = itemEdit.Name;
                    oldBD.Description = itemEdit.Description;
                    oldBD.ToDoListId = itemEdit.ToDoListId;

                    _context.Entry(oldBD).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    return new ModelBasicValidad
                    {
                        Message = "Tarea editada satisfactoriamente.",
                        Type = "success",
                        Valid = true,
                    };
                }
            }
            return new ModelBasicValidad
            {
                Message = "Por favor verifique los datos.",
                Type = "error",
                Valid = false,
            };
        }

        [HttpPut("EndToDoListItem")]
        public async Task<ActionResult<ModelBasicValidad>> EndToDoListItem([FromBody] int id)
        {
            if (ModelState.IsValid)
            {
                ToDoListItem oldBD = await _context.ToDoListItem.FindAsync(id);

                if (oldBD != null && !oldBD.Status)
                {
                    oldBD.Date = DateTime.Now;
                    oldBD.Status = true;

                    _context.Entry(oldBD).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    return new ModelBasicValidad
                    {
                        Message = "Tarea concluida satisfactoriamente.",
                        Type = "success",
                        Valid = true,
                    };
                }
            }
            return new ModelBasicValidad
            {
                Message = "Por favor verifique los datos.",
                Type = "error",
                Valid = false,
            };
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            ToDoListItem item = new ToDoListItem { Id = id };
            _context.Remove(item);
            await _context.SaveChangesAsync();
            return NoContent();
        }
    }
}
