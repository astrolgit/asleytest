﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCubatel.Shared.Models;

namespace TestCubatel.Server.Common
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> option) : base(option) { }

        public DbSet<ToDoList> ToDoList { get; set; }
        public DbSet<ToDoListItem> ToDoListItem { get; set; }
    }
}
