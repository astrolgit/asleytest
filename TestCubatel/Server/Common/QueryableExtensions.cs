﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestCubatel.Shared.Models;

namespace TestCubatel.Server.Common
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> Paged<T>(this IQueryable<T> queryable, Paged paged)
        {
            return queryable.
                Skip((paged.Page - 1) * paged.Quantity)
                .Take(paged.Quantity);
        }
    }
}
