﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCubatel.Server.Common
{
    public static class Auxiliar
    {
        public static string DateString(DateTime date)
        {
            return $"{date.Day} {Month(date.Month)}, {date.Year}";
        }

        public static string Month(int month)
        {
            if (month >= 1 && month <= 12)
            {
                string[] listMonth = {
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Nobiembre",
                "Diciembre"
                };
                return listMonth[month - 1];
            }
            return month.ToString();
        }
    }
}
