﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCubatel.Server.Common
{
    public static class HTTPContextExtensions
    {
        public static async Task InsertarParametrosPaginacionEnRespuesta<T>(this HttpContext context, IQueryable<T> queryable, int cantdadMostrar)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            double counter = await queryable.CountAsync();
            double totalPages = Math.Ceiling(counter / cantdadMostrar);
            context.Response.Headers.Add("totalPagesAstrol", totalPages.ToString());
            context.Response.Headers.Add("TotalItems", counter.ToString());
        }
    }
}
