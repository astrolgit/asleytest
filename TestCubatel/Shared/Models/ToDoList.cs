﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TestCubatel.Shared.Models
{
    [Table("ToDoList")]
    public class ToDoList
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El {0} es requerido.")]
        public string Name { get; set; }

        [Display(Name = "Orden")]
        [RegularExpression("[0-9]{1,}", ErrorMessage = "El {0} es incorrecto.")]
        public int Order { get; set; }
        
        public DateTime Date { get; set; }

        public virtual ICollection<ToDoListItem> ToDoListItem { get; set; }
    }

    public class ToDoListList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public int CountItem { get; set; }
        public string Date { get; set; }
    }
}
