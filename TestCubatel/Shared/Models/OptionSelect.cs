﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCubatel.Shared.Models
{
    public class OptionSelect
    {
        public string Name { get; set; }
        public int Value{ get; set; }
        public bool State{ get; set; }
    }
}
