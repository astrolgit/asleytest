﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCubatel.Shared.Models
{
    public class Paged
    {
        public int Page { get; set; } = 1;
        public int Quantity { get; set; } = 10;
    }
}
