﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TestCubatel.Shared.Models
{
    [Table("ToDoListItem")]
    public class ToDoListItem
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El {0} es requerido.")]
        public string Name { get; set; }

        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "La {0} es requerida.")]
        public string Description { get; set; }

        /// <summary>
        /// false -> Creada
        /// true -> Concluida
        /// </summary>
        [Display(Name = "Estado")]
        public bool Status { get; set; }

        public DateTime Date { get; set; }

        [Display(Name = "Lista")]
        [RegularExpression("[1-9]{1}[0-9]{0,}", ErrorMessage = "La {0} es requerida.")]
        public int ToDoListId { get; set; }

        [ForeignKey("ToDoListId")]
        public virtual ToDoList ToDoList { get; set; }
    }

    public class ToDoListItemList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public string Date { get; set; }
        public string ToDoList { get; set; }
    }
}
