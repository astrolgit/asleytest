﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCubatel.Shared.Models
{
    public class ModelBasicValidad
    {
        public bool Valid { get; set; } = false;
        /// <summary>
        /// success, error, warning, info, question
        /// </summary>
        public string Type { get; set; } = "info";
        public string Message { get; set; } = "";
    }
}
