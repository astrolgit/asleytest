> **Para que el proyecto funcione debe realizar un Update-Database antes de compilarlo, para que cree la Base de Datos. La misma es MS SQL Server Express titulada Test_AstrolDB.**

**En este proyecto usted puede crear tareas y listas de tareas donde cada tarea pertenece a una lista y una lista puede tener muchas tareas.**

- Desde la interfaz de listas usted puede crear, editar, ver las tareas que contiene la misma para el caso de que tengo al menos una y eliminar la lista. Si elimina la lista se eliminan todas las tareas que contenga la misma.
- Desde la interfaz de tareas usted puede crean nuevas tareas, editar las existentes, concluir la misma, así como eliminarla. También se incluye un filtro por lista y nombre de la tarea.

**Ejemplo básico usando .Net Blazor.**

